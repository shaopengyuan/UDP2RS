﻿namespace udp2rs
{
    partial class Form1
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label1 = new System.Windows.Forms.Label();
            this.textBox_udpPort = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label_portState = new System.Windows.Forms.Label();
            this.button_rs232 = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.comboBox_Parity = new System.Windows.Forms.ComboBox();
            this.comboBox_stopBit = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.comboBox_dataBit = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.comboBox_baudrate = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.comboBox_sport = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.textBox_port2 = new System.Windows.Forms.TextBox();
            this.textBox_tgtPort = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.textBox_IP2 = new System.Windows.Forms.TextBox();
            this.textBox_tgtIP = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.textBox_IP = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.button_resend = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.textBox_info = new System.Windows.Forms.ListBox();
            this.button_clear = new System.Windows.Forms.Button();
            this.timer_ShowInfo = new System.Windows.Forms.Timer(this.components);
            this.timer_SendDataUDPPort = new System.Windows.Forms.Timer(this.components);
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.文件ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.保存配置ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.加载配置ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.退出ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveFileDialog_saveConfig = new System.Windows.Forms.SaveFileDialog();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(5, 61);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "本地端口：";
            // 
            // textBox_udpPort
            // 
            this.textBox_udpPort.Location = new System.Drawing.Point(81, 59);
            this.textBox_udpPort.Name = "textBox_udpPort";
            this.textBox_udpPort.Size = new System.Drawing.Size(125, 21);
            this.textBox_udpPort.TabIndex = 1;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label_portState);
            this.groupBox1.Controls.Add(this.button_rs232);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.comboBox_Parity);
            this.groupBox1.Controls.Add(this.comboBox_stopBit);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.comboBox_dataBit);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.comboBox_baudrate);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.comboBox_sport);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Location = new System.Drawing.Point(21, 262);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(212, 181);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "串口设置";
            // 
            // label_portState
            // 
            this.label_portState.AutoSize = true;
            this.label_portState.BackColor = System.Drawing.SystemColors.Control;
            this.label_portState.Location = new System.Drawing.Point(119, 157);
            this.label_portState.Name = "label_portState";
            this.label_portState.Size = new System.Drawing.Size(41, 12);
            this.label_portState.TabIndex = 14;
            this.label_portState.Text = "未打开";
            // 
            // button_rs232
            // 
            this.button_rs232.Location = new System.Drawing.Point(7, 152);
            this.button_rs232.Name = "button_rs232";
            this.button_rs232.Size = new System.Drawing.Size(75, 23);
            this.button_rs232.TabIndex = 13;
            this.button_rs232.Text = "打开串口";
            this.button_rs232.UseVisualStyleBackColor = true;
            this.button_rs232.Click += new System.EventHandler(this.button_rs232_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(3, 124);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(77, 12);
            this.label6.TabIndex = 12;
            this.label6.Text = "校  验  位：";
            // 
            // comboBox_Parity
            // 
            this.comboBox_Parity.FormattingEnabled = true;
            this.comboBox_Parity.Items.AddRange(new object[] {
            "None",
            "Odd",
            "Even",
            "Mark",
            "Space"});
            this.comboBox_Parity.Location = new System.Drawing.Point(81, 120);
            this.comboBox_Parity.Name = "comboBox_Parity";
            this.comboBox_Parity.Size = new System.Drawing.Size(125, 20);
            this.comboBox_Parity.TabIndex = 11;
            // 
            // comboBox_stopBit
            // 
            this.comboBox_stopBit.FormattingEnabled = true;
            this.comboBox_stopBit.Items.AddRange(new object[] {
            "0",
            "1",
            "1.5",
            "2"});
            this.comboBox_stopBit.Location = new System.Drawing.Point(81, 94);
            this.comboBox_stopBit.Name = "comboBox_stopBit";
            this.comboBox_stopBit.Size = new System.Drawing.Size(125, 20);
            this.comboBox_stopBit.TabIndex = 10;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(3, 96);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(77, 12);
            this.label5.TabIndex = 9;
            this.label5.Text = "停  止  位：";
            // 
            // comboBox_dataBit
            // 
            this.comboBox_dataBit.FormattingEnabled = true;
            this.comboBox_dataBit.Items.AddRange(new object[] {
            "5",
            "6",
            "7",
            "8"});
            this.comboBox_dataBit.Location = new System.Drawing.Point(81, 66);
            this.comboBox_dataBit.Name = "comboBox_dataBit";
            this.comboBox_dataBit.Size = new System.Drawing.Size(125, 20);
            this.comboBox_dataBit.TabIndex = 8;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(3, 43);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(77, 12);
            this.label4.TabIndex = 7;
            this.label4.Text = "波  特  率：";
            // 
            // comboBox_baudrate
            // 
            this.comboBox_baudrate.FormattingEnabled = true;
            this.comboBox_baudrate.Items.AddRange(new object[] {
            "75",
            "110",
            "150",
            "300",
            "600",
            "1200",
            "2400",
            "4800",
            "9600",
            "14400",
            "19200",
            "28800",
            "38400",
            "56000",
            "57600",
            "115200",
            "128000",
            "230400",
            "256000"});
            this.comboBox_baudrate.Location = new System.Drawing.Point(81, 40);
            this.comboBox_baudrate.Name = "comboBox_baudrate";
            this.comboBox_baudrate.Size = new System.Drawing.Size(125, 20);
            this.comboBox_baudrate.TabIndex = 6;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 69);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(77, 12);
            this.label3.TabIndex = 5;
            this.label3.Text = "数  据  位：";
            // 
            // comboBox_sport
            // 
            this.comboBox_sport.FormattingEnabled = true;
            this.comboBox_sport.Location = new System.Drawing.Point(81, 17);
            this.comboBox_sport.Name = "comboBox_sport";
            this.comboBox_sport.Size = new System.Drawing.Size(125, 20);
            this.comboBox_sport.TabIndex = 4;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(4, 21);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(77, 12);
            this.label2.TabIndex = 3;
            this.label2.Text = "串口端口号：";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.textBox_port2);
            this.groupBox2.Controls.Add(this.textBox_tgtPort);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.textBox_IP2);
            this.groupBox2.Controls.Add(this.textBox_tgtIP);
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Controls.Add(this.textBox_IP);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.textBox_udpPort);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Location = new System.Drawing.Point(21, 29);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(212, 227);
            this.groupBox2.TabIndex = 3;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "UDP设置";
            // 
            // textBox_port2
            // 
            this.textBox_port2.Location = new System.Drawing.Point(78, 196);
            this.textBox_port2.Name = "textBox_port2";
            this.textBox_port2.Size = new System.Drawing.Size(125, 21);
            this.textBox_port2.TabIndex = 14;
            this.textBox_port2.Text = "8080";
            // 
            // textBox_tgtPort
            // 
            this.textBox_tgtPort.Location = new System.Drawing.Point(78, 128);
            this.textBox_tgtPort.Name = "textBox_tgtPort";
            this.textBox_tgtPort.Size = new System.Drawing.Size(125, 21);
            this.textBox_tgtPort.TabIndex = 10;
            this.textBox_tgtPort.Text = "22345";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(5, 199);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(65, 12);
            this.label10.TabIndex = 13;
            this.label10.Text = "目标端口2:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(5, 131);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(65, 12);
            this.label9.TabIndex = 9;
            this.label9.Text = "目标端口：";
            // 
            // textBox_IP2
            // 
            this.textBox_IP2.Location = new System.Drawing.Point(80, 163);
            this.textBox_IP2.Name = "textBox_IP2";
            this.textBox_IP2.Size = new System.Drawing.Size(123, 21);
            this.textBox_IP2.TabIndex = 12;
            this.textBox_IP2.Text = "192.168.103.32";
            // 
            // textBox_tgtIP
            // 
            this.textBox_tgtIP.Location = new System.Drawing.Point(80, 95);
            this.textBox_tgtIP.Name = "textBox_tgtIP";
            this.textBox_tgtIP.Size = new System.Drawing.Size(125, 21);
            this.textBox_tgtIP.TabIndex = 8;
            this.textBox_tgtIP.Text = "192.168.3.240";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(5, 166);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(53, 12);
            this.label11.TabIndex = 11;
            this.label11.Text = "目标IP2:";
            // 
            // textBox_IP
            // 
            this.textBox_IP.Location = new System.Drawing.Point(81, 23);
            this.textBox_IP.Name = "textBox_IP";
            this.textBox_IP.Size = new System.Drawing.Size(125, 21);
            this.textBox_IP.TabIndex = 5;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(5, 98);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(53, 12);
            this.label8.TabIndex = 7;
            this.label8.Text = "目标IP：";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 26);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(53, 12);
            this.label7.TabIndex = 4;
            this.label7.Text = "本地IP：";
            // 
            // button_resend
            // 
            this.button_resend.Location = new System.Drawing.Point(21, 449);
            this.button_resend.Name = "button_resend";
            this.button_resend.Size = new System.Drawing.Size(212, 31);
            this.button_resend.TabIndex = 4;
            this.button_resend.Text = "监听并转发";
            this.button_resend.UseVisualStyleBackColor = true;
            this.button_resend.Click += new System.EventHandler(this.button_resend_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox3.AutoSize = true;
            this.groupBox3.Controls.Add(this.textBox_info);
            this.groupBox3.Location = new System.Drawing.Point(247, 29);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(421, 488);
            this.groupBox3.TabIndex = 6;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "信息提示区";
            // 
            // textBox_info
            // 
            this.textBox_info.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBox_info.FormattingEnabled = true;
            this.textBox_info.HorizontalScrollbar = true;
            this.textBox_info.ItemHeight = 12;
            this.textBox_info.Location = new System.Drawing.Point(3, 17);
            this.textBox_info.Name = "textBox_info";
            this.textBox_info.Size = new System.Drawing.Size(415, 468);
            this.textBox_info.TabIndex = 7;
            this.textBox_info.SelectedIndexChanged += new System.EventHandler(this.textBox_info_SelectedIndexChanged);
            // 
            // button_clear
            // 
            this.button_clear.Location = new System.Drawing.Point(21, 486);
            this.button_clear.Name = "button_clear";
            this.button_clear.Size = new System.Drawing.Size(212, 31);
            this.button_clear.TabIndex = 7;
            this.button_clear.Text = "清除显示";
            this.button_clear.UseVisualStyleBackColor = true;
            this.button_clear.Click += new System.EventHandler(this.button_clear_Click);
            // 
            // timer_ShowInfo
            // 
            this.timer_ShowInfo.Tick += new System.EventHandler(this.timer_ShowInfo_Tick);
            // 
            // timer_SendDataUDPPort
            // 
            this.timer_SendDataUDPPort.Interval = 50;
            this.timer_SendDataUDPPort.Tick += new System.EventHandler(this.timer_SendDataUDPPort_Tick);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.文件ToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(668, 25);
            this.menuStrip1.TabIndex = 8;
            this.menuStrip1.Text = "文件";
            // 
            // 文件ToolStripMenuItem
            // 
            this.文件ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.保存配置ToolStripMenuItem,
            this.加载配置ToolStripMenuItem,
            this.退出ToolStripMenuItem});
            this.文件ToolStripMenuItem.Name = "文件ToolStripMenuItem";
            this.文件ToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.F)));
            this.文件ToolStripMenuItem.Size = new System.Drawing.Size(44, 21);
            this.文件ToolStripMenuItem.Text = "文件";
            // 
            // 保存配置ToolStripMenuItem
            // 
            this.保存配置ToolStripMenuItem.Name = "保存配置ToolStripMenuItem";
            this.保存配置ToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.S)));
            this.保存配置ToolStripMenuItem.Size = new System.Drawing.Size(163, 22);
            this.保存配置ToolStripMenuItem.Text = "保存配置";
            this.保存配置ToolStripMenuItem.Click += new System.EventHandler(this.保存配置ToolStripMenuItem_Click);
            // 
            // 加载配置ToolStripMenuItem
            // 
            this.加载配置ToolStripMenuItem.Name = "加载配置ToolStripMenuItem";
            this.加载配置ToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.L)));
            this.加载配置ToolStripMenuItem.Size = new System.Drawing.Size(163, 22);
            this.加载配置ToolStripMenuItem.Text = "加载配置";
            // 
            // 退出ToolStripMenuItem
            // 
            this.退出ToolStripMenuItem.Name = "退出ToolStripMenuItem";
            this.退出ToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.F4)));
            this.退出ToolStripMenuItem.Size = new System.Drawing.Size(163, 22);
            this.退出ToolStripMenuItem.Text = "退出";
            // 
            // saveFileDialog_saveConfig
            // 
            this.saveFileDialog_saveConfig.FileOk += new System.ComponentModel.CancelEventHandler(this.saveFileDialog_saveConfig_FileOk);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(668, 522);
            this.Controls.Add(this.button_clear);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.button_resend);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "UDP转RS232";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form1_FormClosed);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox_udpPort;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox comboBox_sport;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox comboBox_baudrate;
        private System.Windows.Forms.ComboBox comboBox_dataBit;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox comboBox_stopBit;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox comboBox_Parity;
        private System.Windows.Forms.Button button_rs232;
        private System.Windows.Forms.Label label_portState;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox textBox_IP;
        private System.Windows.Forms.Button button_resend;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox textBox_tgtIP;
        private System.Windows.Forms.TextBox textBox_tgtPort;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Timer timer_ShowInfo;
        private System.Windows.Forms.ListBox textBox_info;
        private System.Windows.Forms.TextBox textBox_port2;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox textBox_IP2;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button button_clear;
        private System.Windows.Forms.Timer timer_SendDataUDPPort;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem 文件ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 保存配置ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 加载配置ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 退出ToolStripMenuItem;
        private System.Windows.Forms.SaveFileDialog saveFileDialog_saveConfig;
    }
}

